<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
    doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" indent="yes" omit-xml-declaration="no"/>
    
    <!-- load document -->
    <xsl:variable name="tvprogs" select="document('tvprograms.xml')"/>
	<xsl:variable name="beginYr" select="substring($tvprogs/tv/programme[position()=1]/@start,1,4)" />
    <xsl:variable name="beginMn" select="substring($tvprogs/tv/programme[position()=1]/@start,5,2)" />
    <xsl:variable name="beginDy" select="substring($tvprogs/tv/programme[position()=1]/@start,7,2)" />   
    
    <xsl:variable name="endYr" select="substring($tvprogs/tv/programme[position()=last()-2]/@start,1,4)" />
    <xsl:variable name="endMn" select="substring($tvprogs/tv/programme[position()=last()-2]/@start,5,2)" />
    <xsl:variable name="endDy" select="substring($tvprogs/tv/programme[position()=last()-2]/@start,7,2)" />  
    
    <xsl:variable name="Days" select="substring($tvprogs/tv/programme/@start,1,8)" />
    <xsl:variable name="gmt" select="substring($tvprogs/tv/programme/@start|stop, 15)" />
    
    <xsl:variable name="begin" select="concat($beginYr, '-', $beginMn, '-' , $beginDy)" />
    <xsl:variable name="end" select="concat($endYr, '-', $endMn, '-' , $endDy)" />

    <!-- start here -->
    <xsl:template match="/">
		<html>
			<head>
				<title>TV schedule for aljazera.net for week 43 (<xsl:value-of select="concat($begin, ' to ' , $end)" /></title>
				<link>
					<xsl:attribute name="type"><xsl:value-of select="string('text/css')" /></xsl:attribute>
					<xsl:attribute name="rel"><xsl:value-of select="string('stylesheet')" /></xsl:attribute>
					<xsl:attribute name="href"><xsl:value-of select="string('style.css')" /></xsl:attribute>
				</link>
			</head>
			<body>
				<div>
					<xsl:attribute name="id">
						<xsl:value-of select="string('main')" />
					</xsl:attribute>
					
					<!-- extract logo -->
					<img alt="Aljazeera logo">
						<xsl:attribute name="src">
							<xsl:copy-of select="'aljazeera.png'"/>
						</xsl:attribute>
					</img>
					
					<table>
						<caption>
							<xsl:value-of select="concat('TV schedule for ', //@channel,  ' between ' , $begin, ' to ', $end, ' (all time in ', $gmt, ' GMT)')"/>
						</caption>
						<thead>
							<xsl:copy-of select="$header" />
						</thead>
						<tbody>
							<xsl:for-each select="$tvprogs/tv/programme">
							<xsl:variable name="date" select="concat(substring(@start, 1,4), '-', substring(@start, 5,2), '-', substring(@start,7,2)) " />	
							<tr>	
								<td>
									<xsl:value-of select="$date" />
								</td>
								<xsl:apply-templates select="." />
							</tr>
							</xsl:for-each>
						</tbody>
					</table>
					<xsl:copy-of select="$footer" />
				</div>
			</body>
		</html>
    </xsl:template>
    
    <!-- make table header -->
    <xsl:variable name="header">
		<tr>
			<th>Date</th>
			<th>Start</th>
			<th>Stop</th>
			<th>Title</th>
			<th>Description</th>
		</tr>
    </xsl:variable>
    
    <!-- template for title -->
    <xsl:template match="title">
		<xsl:attribute name="lang">
			<xsl:value-of select="@lang"/>
		</xsl:attribute>
		<xsl:value-of select="."/>
	</xsl:template>
	
	<!-- template for description -->
	<xsl:template match="desc">
		<xsl:attribute name="lang">
			<xsl:value-of select="@lang"/>
		</xsl:attribute>
		<xsl:value-of select="."/>
	</xsl:template>
	
	<!-- template for each program item in a row -->
	<xsl:template match="programme">
		<td><xsl:value-of select="concat(substring(@start,9,2),':',substring(@start,11,2))" /></td>
		<td><xsl:value-of select="concat(substring(@stop,9,2),':',substring(@stop,11,2))" /></td>
		<td><xsl:apply-templates select="title" /></td>
		<td><xsl:apply-templates select="desc" /></td>
	</xsl:template>
	
	<!-- footer -->
	<xsl:variable name="footer">
		<p>
			<xsl:attribute name="class"><xsl:value-of select="string('footer')" /></xsl:attribute>
		This work is brought to you by <br />
		Bernard Che Longho <br />
		lobe1602@student.miun.se
		</p>
		
	</xsl:variable>
</xsl:stylesheet>
