xquery version "1.0";
declare namespace ext="http://www.altova.com/xslt-extensions";
(: 
	@brief make a function to extract the date in the format yyyy-mm-dd
	@para string (start|stop attribute of programme
	@return yyyy-mm-dd (string)
:)
declare function local:get_date($s as xs:string?)
	as xs:string?
	{
		let $start_year := substring($s, 1,4)
		let $start_month := substring($s, 5,2)
		let $start_day := substring($s,7,2)
		return concat($start_year, '-', $start_month, '-', $start_day)
	};

(: 
	@brief get first start date
	@return first program start attribute (string)	
:)
declare function local:start()
	as xs:string?
	{
		data(doc("tvprograms.xml")/tv/programme[position()=1]/@start)
	};

(: 
	@brief get end date
	@return last but 2 program start attribute (string)	
:)
declare function local:end()
	as xs:string?
	{
		data(doc("tvprograms.xml")/tv/programme[position()=last()-2]/@start)
	};

declare function local:channel()
	as xs:string?
	{
		data(doc("tvprograms.xml")/tv/programme[1]/@channel)
	};
	
(:
	@brief Extract time in the format hh:mm
	@param start attribute for a program
:)
declare function local:time($s as xs:string?)
	as xs:string?
	{
		concat(substring($s,9,2), ':', substring($s,11,2))
	};

(:
	@brief Make the table rows
	@return A table with date, startTime, stopTime, title and description 
			of each program node in the combined TV programs file
:)
declare function local:makeRows()
	{
		let $file := doc("tvprograms.xml")
		for $p in $file/tv/programme 
		return 
		<tr>
			<td>{local:get_date($p/@start)}</td>
			<td>{local:time($p/@start)} </td>
			<td>{local:time($p/@stop)} </td>
			<td lang="{data($p/title/@lang)}">{data($p/title)}</td>
			<td lang="{data($p/desc/@lang)}">{data($p/desc)}</td>
		</tr>
	};
	
<html>
	<head>
	<title>Testing xquery</title>
	<link type="text/css" rel="stylesheet" href="style.css" />
	</head>
	<body>
		<img src="aljazeera.png" alt="aljazera.net logo" />
		<table>
		
			<caption> 
			TV schedule for {local:channel()} between {local:get_date(local:start())} to {local:get_date(local:end())}
			</caption>
			<thead>
				<tr>
					<th>Date</th>
					<th>Start</th>
					<th>Stop</th>
					<th>Title</th>
					<th>Description</th>
				</tr>
			</thead>
			<tbody>
				{local:makeRows()}
			</tbody>
	</table>
	<p class="footer">
		This work is brought to you by <br />
		Bernard Che Longho <br />
		lobe1602@student.miun.se
		</p>
	</body>
	
</html>
		