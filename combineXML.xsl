<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<!-- get all the files -->
	<xsl:variable name="tv22" select="document('aljazeera.net_2017-10-22.xml')"/>
	<xsl:variable name="tv23" select="document('aljazeera.net_2017-10-23.xml')"/>
	<xsl:variable name="tv24" select="document('aljazeera.net_2017-10-24.xml')"/>
	<xsl:variable name="tv25" select="document('aljazeera.net_2017-10-25.xml')"/>
	<xsl:variable name="tv26" select="document('aljazeera.net_2017-10-26.xml')"/>
	<xsl:variable name="tv27" select="document('aljazeera.net_2017-10-27.xml')"/>
	<xsl:variable name="tv28" select="document('aljazeera.net_2017-10-28.xml')"/>
	
	<!-- Run the merge -->
	<xsl:template match="/">
		<tv generator-info-name="Taiga XMLTV" generator-info-url="http://xmltv.se" 
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
				xsi:noNamespaceSchemaLocation="schema.xsd">
			<xsl:copy-of select="$tv22/tv/*" />
			<xsl:copy-of select="$tv23/tv/*" />
			<xsl:copy-of select="$tv24/tv/*" />
			<xsl:copy-of select="$tv25/tv/*" />
			<xsl:copy-of select="$tv26/tv/*" />
			<xsl:copy-of select="$tv27/tv/*" />
			<xsl:copy-of select="$tv28/tv/*" />
		</tv>
	</xsl:template>
</xsl:stylesheet>
